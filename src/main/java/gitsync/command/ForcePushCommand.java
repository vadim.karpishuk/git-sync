package gitsync.command;

import gitsync.Config;
import gitsync.GitSync;
import gitsync.RepoService;
import gitsync.Repository;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static gitsync.RepoService.*;

public class ForcePushCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.push")) {
            RepoService repoService = new RepoService();
            if(args.length == 0) {
                commandSender.sendMessage(ChatColor.DARK_RED + "Should be specified one of repos to push!");
                return false;
            }
            Repository repository = Config.getInstance().findRepoByName(args[0]);
            if(repository == null) {
                commandSender.sendMessage(ChatColor.DARK_RED + String.format("Can't find repo with name %s", args[0]));
                return false;
            }
            if(repository.getRemote().equals("empty")) {
                commandSender.sendMessage(ChatColor.YELLOW + String.format("Can't push repo %s because there is no remote repo", repository.getName()));
                return false;
            }
            GitSync.getInstance().getLogger().info(String.format("Trying to push force. Applier: %s", commandSender.getName()));
            repoService.addChangesToCommit(repository, GitSync.getInstance().getLogger());
            if(args.length > 2 && args[1].equals("message")) {
                List<String> argsWithoutFirstTwo = new ArrayList<>(Arrays.asList(args));
                argsWithoutFirstTwo.remove(0);
                argsWithoutFirstTwo.remove(0);
                String customMessage = "";
                for(String part: argsWithoutFirstTwo) {
                    customMessage = customMessage.concat(part + " ");
                }
                repoService.createCommit(repository, GitSync.getInstance().getLogger(), SERVER_COMMIT_TO_MAKE + " " + customMessage);
            } else {
                repoService.createCommit(repository, GitSync.getInstance().getLogger(), SERVER_COMMIT_TO_MAKE);
            }
            List<String> output = repoService.pushForce(repository, GitSync.getInstance().getLogger());
            if(repoService.isOutputContains(output, NO_CHANGES_TO_PUSH)) {
                commandSender.sendMessage(ChatColor.GREEN + String.format("No forced changes in local repo of %s detected. Remote repos is already up-to-date", repository.getName()));
                GitSync.getInstance().getLogger()
                        .info(String.format("No changes forced in local repo of %s detected. Remote repos is already up-to-date. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            if(repoService.isOutputContains(output, FORCED_UPDATE_SUCCESS)) {
                commandSender.sendMessage(ChatColor.GREEN + String.format("Force push successfully applied for %s", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Force push successfully applied for %s. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            if(repoService.isOutputContains(output, FAILED_TO_PUSH_SOME_REFS)) {
                commandSender.sendMessage(ChatColor.DARK_RED + String.format("Not allowed to apply force push for %s (branch protected)", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Not allowed to apply force push for %s (branch protected). Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            commandSender.sendMessage(ChatColor.YELLOW + String.format("Unknown type of output for system. Please check output by yourself to validate that is everything is OK (%s)", repository.getName()));
            GitSync.getInstance().getLogger().info(String.format("Unknown type of output for system. Please check output by yourself to validate that is everything is OK (%s). Applier: %s", repository.getName(), commandSender.getName()));
        } else {
            commandSender.sendMessage(ChatColor.DARK_RED + "Not enough permissions to do that!");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.push")) {
            List<String> reposNames = Config.getInstance().getRepositories().stream()
                    .filter(repository -> repository.isEnabled() && !repository.getRemote().equals("empty")).map(Repository::getName)
                    .collect(Collectors.toList());
            if (args.length == 1) {
                return reposNames.stream().filter(string -> string.startsWith(args[0])).collect(Collectors.toList());
            }
            if(args.length == 2) {
                return Collections.singletonList("message");
            }
        }
        return null;
    }
}

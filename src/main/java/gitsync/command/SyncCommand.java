package gitsync.command;

import gitsync.Config;
import gitsync.GitSync;
import gitsync.RepoService;
import gitsync.Repository;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static gitsync.RepoService.SERVER_COMMIT_TO_MAKE;

public class SyncCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.sync")) {
            RepoService repoService = new RepoService();
            if(args.length == 0) {
                commandSender.sendMessage(ChatColor.DARK_RED + "Should be specified one of repos to push!");
                return false;
            }
            Repository repository = Config.getInstance().findRepoByName(args[0]);
            if(repository == null) {
                commandSender.sendMessage(ChatColor.DARK_RED + String.format("Can't find repo with name %s", args[0]));
                return false;
            }
            if(repository.getRemote().equals("empty")) {
                commandSender.sendMessage(ChatColor.YELLOW + String.format("Can't sync repo %s because there is no remote repo", repository.getName()));
                return false;
            }
            repoService.addChangesToCommit(repository, GitSync.getInstance().getLogger());
            if(args.length > 2 && args[1].equals("message")) {
                List<String> argsWithoutFirstTwo = new ArrayList<>(Arrays.asList(args));
                argsWithoutFirstTwo.remove(0);
                argsWithoutFirstTwo.remove(0);
                String customMessage = "";
                for(String part: argsWithoutFirstTwo) {
                    customMessage = customMessage.concat(part + " ");
                }
                repoService.createCommit(repository, GitSync.getInstance().getLogger(), SERVER_COMMIT_TO_MAKE + " " + customMessage);
            } else {
                repoService.createCommit(repository, GitSync.getInstance().getLogger(), SERVER_COMMIT_TO_MAKE);
            }
            if(repoService.favorableSync(repository, GitSync.getInstance().getLogger())) {
                commandSender.sendMessage(ChatColor.GREEN + String.format("Sync for %s repo successfully applied", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Sync for %s repo successfully applied. Applier: %s", repository.getName(), commandSender.getName()));
            } else {
                commandSender.sendMessage(ChatColor.DARK_RED
                        + String.format("Sync can't be applied because of reasons like conflict in local merge of %s and etc. Use manual push and pull, but your changes in remote will be overwrited!", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Sync for repo %s failed. Applier: %s", repository.getName(), commandSender.getName()));
            }
        } else {
            commandSender.sendMessage(ChatColor.DARK_RED + "Not enough permissions to do that!");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.sync")) {
            List<String> reposNames = Config.getInstance().getRepositories().stream()
                    .filter(repository -> repository.isEnabled() && !repository.getRemote().equals("empty")).map(Repository::getName)
                    .collect(Collectors.toList());
            if (args.length == 1) {
                return reposNames.stream().filter(string -> string.startsWith(args[0])).collect(Collectors.toList());
            }
            if(args.length == 2) {
                return Collections.singletonList("message");
            }
        }
        return null;
    }
}

package gitsync.command;

import gitsync.Config;
import gitsync.GitSync;
import gitsync.RepoService;
import gitsync.Repository;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.List;
import java.util.stream.Collectors;

import static gitsync.RepoService.*;

public class PullCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.pull")) {
            RepoService repoService = new RepoService();
            if(args.length == 0) {
                commandSender.sendMessage(ChatColor.DARK_RED + "Should be specified one of repos to pull!");
                return false;
            }
            Repository repository = Config.getInstance().findRepoByName(args[0]);
            if(repository == null) {
                commandSender.sendMessage(ChatColor.DARK_RED + String.format("Can't find repo with name %s", args[0]));
                return false;
            }
            if(repository.getRemote().equals("empty")) {
                commandSender.sendMessage(ChatColor.YELLOW + String.format("Can't pull repo %s because there is no remote repo", repository.getName()));
                return false;
            }
            GitSync.getInstance().getLogger().info(String.format("Trying to pull. Applier: %s", commandSender.getName()));
            repoService.addChangesToCommit(repository, GitSync.getInstance().getLogger());
            repoService.createCommit(repository, GitSync.getInstance().getLogger(), SERVER_COMMIT_TO_MAKE);
            List<String> output = repoService.pull(repository, GitSync.getInstance().getLogger());
            if(repoService.isOutputContains(output, AUTOMATIC_MERGE_FAILED)) {
                commandSender.sendMessage(ChatColor.DARK_RED + String.format("Can't merge remote and local to sync them, will abort merge for %s", repository.getName()));
                repoService.abortMerge(repository, GitSync.getInstance().getLogger());
                commandSender.sendMessage(ChatColor.YELLOW + String.format("Merge for pull of %s is aborted, use manual push and pull again", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Automatic merge failed and aborted for %s. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            if(repoService.isOutputContains(output, AUTOMATIC_MERGE_SUCCESS)) {
                repoService.createCommit(repository, GitSync.getInstance().getLogger(), "'[merged]'");
                repoService.push(repository, GitSync.getInstance().getLogger());
                commandSender.sendMessage(ChatColor.GREEN + String.format("Successful pull of %s to local repo, also automatic merge and push applied", repository.getName()));
                GitSync.getInstance().getLogger()
                        .info(String.format("Successful pull of %s to local repo, also automatic merge and push applied. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            if(repoService.isOutputContains(output, NO_UPDATES_FROM_LOCAL_MANY) || repoService.isOutputContains(output, NO_UPDATES_FROM_LOCAL_ONE)) {
                commandSender.sendMessage(ChatColor.GREEN + String.format("Successful pull of %s to local repo", repository.getName()));
                GitSync.getInstance().getLogger().info(String.format("Successful pull of %s to local repo. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            if(repoService.isOutputContains(output, NO_UPDATES_FROM_REMOTE)) {
                commandSender.sendMessage(ChatColor.GREEN + String.format("No changes in remote repo of %s detected. Local repos is already up-to-date", repository.getName()));
                GitSync.getInstance().getLogger()
                        .info(String.format("No changes in remote repo of %s detected. Local repos is already up-to-date. Applier: %s", repository.getName(), commandSender.getName()));
                return true;
            }
            commandSender.sendMessage(ChatColor.YELLOW + String.format("Unknown type of output for system. Please check output by yourself to validate that is everything is OK (%s)", repository.getName()));
            GitSync.getInstance().getLogger().info(String.format("Unknown type of output for system. Please check output by yourself to validate that is everything is OK (%s). Applier: %s", repository.getName(), commandSender.getName()));
        } else {
            commandSender.sendMessage(ChatColor.DARK_RED + "Not enough permissions to do that!");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.hasPermission("gs.pull")) {
            List<String> reposNames = Config.getInstance().getRepositories().stream()
                    .filter(repository -> repository.isEnabled() && !repository.getRemote().equals("empty")).map(Repository::getName)
                    .collect(Collectors.toList());
            if (args.length == 1) {
                return reposNames.stream().filter(string -> string.startsWith(args[0])).collect(Collectors.toList());
            }
        }
        return null;
    }
}

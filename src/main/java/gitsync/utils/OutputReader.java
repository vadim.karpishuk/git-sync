package gitsync.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class OutputReader {

    private final InputStream inputStream;

    public OutputReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List<String> getOutput() {
        List<String> output = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                output.add(line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return output;
    }

}

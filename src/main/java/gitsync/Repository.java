package gitsync;

import java.io.File;
import java.util.List;

public class Repository {

    private String name;
    private boolean enabled;
    private String remote;
    private boolean localRepoCreated;
    private File directory;
    private List<String> ignoreList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getRemote() {
        return remote;
    }

    public void setRemote(String remote) {
        this.remote = remote;
    }

    public boolean isLocalRepoCreated() {
        return localRepoCreated;
    }

    public void setLocalRepoCreated(boolean localRepoCreated) {
        this.localRepoCreated = localRepoCreated;
    }

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public List<String> getIgnoreList() {
        return ignoreList;
    }

    public void setIgnoreList(List<String> ignoreList) {
        this.ignoreList = ignoreList;
    }
}

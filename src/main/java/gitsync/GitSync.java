package gitsync;

import gitsync.command.ForcePushCommand;
import gitsync.command.PullCommand;
import gitsync.command.ReloadCommand;
import gitsync.command.SyncCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GitSync extends JavaPlugin {

    private static GitSync instance;

    @Override
    public void onEnable() {
        // All you have to do is adding the following two lines in your onEnable method.
        // You can find the plugin ids of your plugins on the page https://bstats.org/what-is-my-plugin-id
        int pluginId = 14817; // <-- Replace with the id of your plugin!
        Metrics metrics = new Metrics(this, pluginId);
        instance = this;
        loadDataIntoMemory();
        RepoService repoService = new RepoService();
        repoService.dailySync(getLogger());
        getCommand("gsreload").setExecutor(new ReloadCommand());
        getCommand("gssync").setExecutor(new SyncCommand());
        getCommand("gspush").setExecutor(new ForcePushCommand());
        getCommand("gspull").setExecutor(new PullCommand());
    }

    public void loadDataIntoMemory() {
        Config.clear();
        File configFile = checkConfig(getDataFolder());
        loadConfig(configFile, getDataFolder().getParentFile());
        RepoService repoService = new RepoService();
        repoService.createReposWhereNeeded(getDataFolder().getParentFile(), getLogger());
        repoService.linkRemotesAndLocals(getLogger());
        repoService.recreateGitIgnores(getLogger());
    }

    public static GitSync getInstance() {
        return instance;
    }

    private File checkConfig(File directory) {
        if(!directory.exists()) {
            directory.mkdir();
        }
        File config = new File(directory, "config.yml");
        if(!config.exists()) {
            try {
                config.createNewFile();
            } catch (IOException e) {
                getLogger().warning("Cannot create main config!");
            }
        }
        List<String> names = getNamesOfEachPluginFolder(Arrays.asList(directory.getParentFile().listFiles()).stream().filter(File::isDirectory).collect(Collectors.toList()));
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(config);
        for(String name: names) {
            if(!fileConfiguration.contains(name)) {
                fileConfiguration.createSection(name);
            }
            if(!fileConfiguration.contains(name + ".enabled")) {
                fileConfiguration.set(name + ".enabled", false);
            }
            if(!fileConfiguration.contains(name + ".remote")) {
                fileConfiguration.set(name + ".remote", "empty");
            }
        }
        if(!fileConfiguration.contains("dailySync")) {
            fileConfiguration.set("dailySync", Config.Scenario.ALL.toString());
        }
        try {
            fileConfiguration.save(config);
        } catch (IOException e) {
            getLogger().warning("Cannot save main config!");
        }
        return config;
    }

    private void loadConfig(File configFile, File dirOfPlugins) {
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
        for(String pluginSection: fileConfiguration.getConfigurationSection("").getKeys(false)) {
            File dirOfSomePlugin = new File(dirOfPlugins, pluginSection);
            if(!dirOfSomePlugin.exists()) {
                continue;
            }
            Repository repository = new Repository();
            repository.setEnabled(fileConfiguration.getBoolean(pluginSection + ".enabled"));
            repository.setName(pluginSection);
            repository.setRemote(fileConfiguration.getString(pluginSection + ".remote"));
            repository.setLocalRepoCreated(isLocalRepoCreated(dirOfSomePlugin));
            repository.setDirectory(dirOfSomePlugin);
            if(fileConfiguration.contains(pluginSection + ".exclude")) {
                repository.setIgnoreList(fileConfiguration.getStringList(pluginSection + ".exclude"));
            } else {
                repository.setIgnoreList(new ArrayList<>());
            }
            Config.getInstance().getRepositories().add(repository);
        }
        Config.getInstance().setScenarioWhileDailySync(Config.Scenario.valueOf(fileConfiguration.getString("dailySync")));
    }

    private boolean isLocalRepoCreated(File dirOfSomePlugin) {
        List<File> filesInSomePluginDir = Arrays.asList(dirOfSomePlugin.listFiles());
        for(File eachFile: filesInSomePluginDir) {
            if(eachFile.getName().equals(".git")) {
                return true;
            }
        }
        return false;
    }

    private List<String> getNamesOfEachPluginFolder(List<File> files) {
        return files.stream().map(File::getName).collect(Collectors.toList());
    }

}

package gitsync;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Config {

    private static Config instance;
    private List<Repository> repositories;
    private Scenario scenarioWhileDailySync;

    private Config() {
        repositories = new ArrayList<>();
    }

    public static Config getInstance() {
        if(instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public static void clear() {
        instance = new Config();
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public Scenario getScenarioWhileDailySync() {
        return scenarioWhileDailySync;
    }

    public void setScenarioWhileDailySync(Scenario scenarioWhileDailySync) {
        this.scenarioWhileDailySync = scenarioWhileDailySync;
    }

    public enum Scenario {
        FAVORABLE,
        FORCE,
        ALL
    }

    public Repository findRepoByName(String name) {
        Optional<Repository> repositoryToGive = repositories.stream().filter(repository -> repository.getName().equals(name)).findFirst();
        return repositoryToGive.orElse(null);
    }
}
